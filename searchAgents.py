# searchAgents.py
# ---------------
# Licensing Information:  You are free to use or extend these projects for
# educational purposes provided that (1) you do not distribute or publish
# solutions, (2) you retain this notice, and (3) you provide clear
# attribution to UC Berkeley, including a link to http://ai.berkeley.edu.
#
# Attribution Information: The Pacman AI projects were developed at UC Berkeley.
# The core projects and autograders were primarily created by John DeNero
# (denero@cs.berkeley.edu) and Dan Klein (klein@cs.berkeley.edu).
# Student side autograding was added by Brad Miller, Nick Hay, and
# Pieter Abbeel (pabbeel@cs.berkeley.edu).


"""
This file contains all of the agents that can be selected to control Pacman.  To
select an agent, use the '-p' option when running pacman.py.  Arguments can be
passed to your agent using '-a'.  For example, to load a SearchAgent that uses
depth first search (dfs), run the following command:

> python pacman.py -p SearchAgent -a fn=depthFirstSearch

Commands to invoke other search strategies can be found in the project
description.

Please only change the parts of the file you are asked to.  Look for the lines
that say

"*** YOUR CODE HERE ***"

The parts you fill in start about 3/4 of the way down.  Follow the project
description for details.

Good luck and happy searching!
"""

from game import Directions
from game import Agent
from game import Actions
import util
import time
import search
from Queue import Queue

# Minimum Spanning Tree Code
# downloaded from: https://github.com/silky/PADS-mirror/blob/master/MinimumSpanningTree.py
# spanning tree code under MIT License:
#
# Copyright (c) 2002-2015, David Eppstein
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.

class UnionFind:
    """Union-find data structure.

    Each unionFind instance X maintains a family of disjoint sets of
    hashable objects, supporting the following two methods:

    - X[item] returns a name for the set containing the given item.
      Each set is named by an arbitrarily-chosen one of its members; as
      long as the set remains unchanged it will keep the same name. If
      the item is not yet part of a set in X, a new singleton set is
      created for it.

    - X.union(item1, item2, ...) merges the sets containing each item
      into a single larger set.  If any item is not yet part of a set
      in X, it is added to X as one of the members of the merged set.
    """

    def __init__(self):
        """Create a new empty union-find structure."""
        self.weights = {}
        self.parents = {}

    def __getitem__(self, object):
        """Find and return the name of the set containing the object."""

        # check for previously unknown object
        if object not in self.parents:
            self.parents[object] = object
            self.weights[object] = 1
            return object

        # find path of objects leading to the root
        path = [object]
        root = self.parents[object]
        while root != path[-1]:
            path.append(root)
            root = self.parents[root]

        # compress the path and return
        for ancestor in path:
            self.parents[ancestor] = root
        return root

    def __iter__(self):
        """Iterate through all items ever found or unioned by this structure."""
        return iter(self.parents)

    def union(self, *objects):
        """Find the sets containing the objects and merge them all."""
        roots = [self[x] for x in objects]
        heaviest = max([(self.weights[r],r) for r in roots])[1]
        for r in roots:
            if r != heaviest:
                self.weights[heaviest] += self.weights[r]
                self.parents[r] = heaviest

def MinimumSpanningTree(G):
    """
    Return the minimum spanning tree of an undirected graph G.
    G should be represented in such a way that iter(G) lists its
    vertices, iter(G[u]) lists the neighbors of u, G[u][v] gives the
    length of edge u,v, and G[u][v] should always equal G[v][u].
    The tree is returned as a list of edges.
    """

    # Kruskal's algorithm: sort edges by weight, and add them one at a time.
    # We use Kruskal's algorithm, first because it is very simple to
    # implement once UnionFind exists, and second, because the only slow
    # part (the sort) is sped up by being built in to Python.
    subtrees = UnionFind()
    tree = []
    for W,u,v in sorted((G[u][v],u,v) for u in G for v in G[u]):
        if subtrees[u] != subtrees[v]:
            tree.append((u,v))
            subtrees.union(u,v)
    return tree

# END CODE SPANNING TREE BY David Eppstein


class GoWestAgent(Agent):
    "An agent that goes West until it can't."

    def getAction(self, state):
        "The agent receives a GameState (defined in pacman.py)."
        if Directions.WEST in state.getLegalPacmanActions():
            return Directions.WEST
        else:
            return Directions.STOP


#######################################################
# This portion is written for you, but will only work #
#       after you fill in parts of search.py          #
#######################################################

class SearchAgent(Agent):
    """
    This very general search agent finds a path using a supplied search
    algorithm for a supplied search problem, then returns actions to follow that
    path.

    As a default, this agent runs DFS on a PositionSearchProblem to find
    location (1,1)

    Options for fn include:
      depthFirstSearch or dfs
      breadthFirstSearch or bfs


    Note: You should NOT change any code in SearchAgent
    """

    def __init__(self, fn='depthFirstSearch', prob='PositionSearchProblem', heuristic='nullHeuristic'):
        # Warning: some advanced Python magic is employed below to find the right functions and problems

        # Get the search function from the name and heuristic
        if fn not in dir(search):
            raise AttributeError, fn + ' is not a search function in search.py.'
        func = getattr(search, fn)
        if 'heuristic' not in func.func_code.co_varnames:
            print('[SearchAgent] using function ' + fn)
            self.searchFunction = func
        else:
            if heuristic in globals().keys():
                heur = globals()[heuristic]
            elif heuristic in dir(search):
                heur = getattr(search, heuristic)
            else:
                raise AttributeError, heuristic + ' is not a function in searchAgents.py or search.py.'
            print('[SearchAgent] using function %s and heuristic %s' % (fn, heuristic))
            # Note: this bit of Python trickery combines the search algorithm and the heuristic
            self.searchFunction = lambda x: func(x, heuristic=heur)

        # Get the search problem type from the name
        if prob not in globals().keys() or not prob.endswith('Problem'):
            raise AttributeError, prob + ' is not a search problem type in SearchAgents.py.'
        self.searchType = globals()[prob]
        print('[SearchAgent] using problem type ' + prob)

    def registerInitialState(self, state):
        """
        This is the first time that the agent sees the layout of the game
        board. Here, we choose a path to the goal. In this phase, the agent
        should compute the path to the goal and store it in a local variable.
        All of the work is done in this method!

        state: a GameState object (pacman.py)
        """
        if self.searchFunction == None: raise Exception, "No search function provided for SearchAgent"
        starttime = time.time()
        problem = self.searchType(state)  # Makes a new search problem
        self.actions = self.searchFunction(problem)  # Find a path
        totalCost = problem.getCostOfActions(self.actions)
        print('Path found with total cost of %d in %.1f seconds' % (totalCost, time.time() - starttime))
        if '_expanded' in dir(problem): print('Search nodes expanded: %d' % problem._expanded)

    def getAction(self, state):
        """
        Returns the next action in the path chosen earlier (in
        registerInitialState).  Return Directions.STOP if there is no further
        action to take.

        state: a GameState object (pacman.py)
        """
        if 'actionIndex' not in dir(self): self.actionIndex = 0
        i = self.actionIndex
        self.actionIndex += 1
        if i < len(self.actions):
            return self.actions[i]
        else:
            return Directions.STOP


class PositionSearchProblem(search.SearchProblem):
    """
    A search problem defines the state space, start state, goal test, successor
    function and cost function.  This search problem can be used to find paths
    to a particular point on the pacman board.

    The state space consists of (x,y) positions in a pacman game.

    Note: this search problem is fully specified; you should NOT change it.
    """

    def __init__(self, gameState, costFn=lambda x: 1, goal=(1, 1), start=None, warn=True, visualize=True):
        """
        Stores the start and goal.

        gameState: A GameState object (pacman.py)
        costFn: A function from a search state (tuple) to a non-negative number
        goal: A position in the gameState
        """
        self.walls = gameState.getWalls()
        self.startState = gameState.getPacmanPosition()
        if start != None: self.startState = start
        self.goal = goal
        self.costFn = costFn
        self.visualize = visualize
        if warn and (gameState.getNumFood() != 1 or not gameState.hasFood(*goal)):
            print 'Warning: this does not look like a regular search maze'

        # For display purposes
        self._visited, self._visitedlist, self._expanded = {}, [], 0  # DO NOT CHANGE

    def getStartState(self):
        return self.startState

    def isGoalState(self, state):
        isGoal = state == self.goal

        # For display purposes only
        if isGoal and self.visualize:
            self._visitedlist.append(state)
            import __main__
            if '_display' in dir(__main__):
                if 'drawExpandedCells' in dir(__main__._display):  # @UndefinedVariable
                    __main__._display.drawExpandedCells(self._visitedlist)  # @UndefinedVariable

        return isGoal

    def getSuccessors(self, state):
        """
        Returns successor states, the actions they require, and a cost of 1.

         As noted in search.py:
             For a given state, this should return a list of triples,
         (successor, action, stepCost), where 'successor' is a
         successor to the current state, 'action' is the action
         required to get there, and 'stepCost' is the incremental
         cost of expanding to that successor
        """

        successors = []
        for action in [Directions.NORTH, Directions.SOUTH, Directions.EAST, Directions.WEST]:
            x, y = state
            dx, dy = Actions.directionToVector(action)
            nextx, nexty = int(x + dx), int(y + dy)
            if not self.walls[nextx][nexty]:
                nextState = (nextx, nexty)
                cost = self.costFn(nextState)
                successors.append((nextState, action, cost))

        # Bookkeeping for display purposes
        self._expanded += 1  # DO NOT CHANGE
        if state not in self._visited:
            self._visited[state] = True
            self._visitedlist.append(state)

        return successors

    def getCostOfActions(self, actions):
        """
        Returns the cost of a particular sequence of actions. If those actions
        include an illegal move, return 999999.
        """
        if actions == None: return 999999
        x, y = self.getStartState()
        cost = 0
        for action in actions:
            # Check figure out the next state and see whether its' legal
            dx, dy = Actions.directionToVector(action)
            x, y = int(x + dx), int(y + dy)
            if self.walls[x][y]: return 999999
            cost += self.costFn((x, y))
        return cost


class StayEastSearchAgent(SearchAgent):
    """
    An agent for position search with a cost function that penalizes being in
    positions on the West side of the board.

    The cost function for stepping into a position (x,y) is 1/2^x.
    """

    def __init__(self):
        self.searchFunction = search.uniformCostSearch
        costFn = lambda pos: .5 ** pos[0]
        self.searchType = lambda state: PositionSearchProblem(state, costFn, (1, 1), None, False)


class StayWestSearchAgent(SearchAgent):
    """
    An agent for position search with a cost function that penalizes being in
    positions on the East side of the board.

    The cost function for stepping into a position (x,y) is 2^x.
    """

    def __init__(self):
        self.searchFunction = search.uniformCostSearch
        costFn = lambda pos: 2 ** pos[0]
        self.searchType = lambda state: PositionSearchProblem(state, costFn)


def manhattanHeuristic(position, problem, info={}):
    "The Manhattan distance heuristic for a PositionSearchProblem"
    xy1 = position
    xy2 = problem.goal
    return abs(xy1[0] - xy2[0]) + abs(xy1[1] - xy2[1])


def euclideanHeuristic(position, problem, info={}):
    "The Euclidean distance heuristic for a PositionSearchProblem"
    xy1 = position
    xy2 = problem.goal
    return ((xy1[0] - xy2[0]) ** 2 + (xy1[1] - xy2[1]) ** 2) ** 0.5


#####################################################
# This portion is incomplete.  Time to write code!  #
#####################################################

class CornersProblem(search.SearchProblem):
    """
    This search problem finds paths through all four corners of a layout.

    You must select a suitable state space and successor function
    """

    def __init__(self, startingGameState):
        """
        Stores the walls, pacman's starting position and corners.
        """
        self.walls = startingGameState.getWalls()
        self.startingPosition = startingGameState.getPacmanPosition()
        top, right = self.walls.height - 2, self.walls.width - 2
        self.corners = ((1, 1), (1, top), (right, 1), (right, top))
        for corner in self.corners:
            if not startingGameState.hasFood(*corner):
                print 'Warning: no food in corner ' + str(corner)
        self._expanded = 0  # DO NOT CHANGE; Number of search nodes expanded
        # Please add any code here which you would like to use
        # in initializing the problem
        "*** YOUR CODE HERE ***"

    def getStartState(self):
        """
        Returns the start state (in your state space, not the full Pacman state
        space)
        """
        "*** YOUR CODE HERE ***"
        util.raiseNotDefined()

    def isGoalState(self, state):
        """
        Returns whether this search state is a goal state of the problem.
        """
        "*** YOUR CODE HERE ***"
        util.raiseNotDefined()

    def getSuccessors(self, state):
        """
        Returns successor states, the actions they require, and a cost of 1.

         As noted in search.py:
            For a given state, this should return a list of triples, (successor,
            action, stepCost), where 'successor' is a successor to the current
            state, 'action' is the action required to get there, and 'stepCost'
            is the incremental cost of expanding to that successor
        """

        successors = []
        for action in [Directions.NORTH, Directions.SOUTH, Directions.EAST, Directions.WEST]:
            # Add a successor state to the successor list if the action is legal
            # Here's a code snippet for figuring out whether a new position hits a wall:
            #   x,y = currentPosition
            #   dx, dy = Actions.directionToVector(action)
            #   nextx, nexty = int(x + dx), int(y + dy)
            #   hitsWall = self.walls[nextx][nexty]

            "*** YOUR CODE HERE ***"

        self._expanded += 1  # DO NOT CHANGE
        return successors

    def getCostOfActions(self, actions):
        """
        Returns the cost of a particular sequence of actions.  If those actions
        include an illegal move, return 999999.  This is implemented for you.
        """
        if actions == None: return 999999
        x, y = self.startingPosition
        for action in actions:
            dx, dy = Actions.directionToVector(action)
            x, y = int(x + dx), int(y + dy)
            if self.walls[x][y]: return 999999
        return len(actions)


def cornersHeuristic(state, problem):
    """
    A heuristic for the CornersProblem that you defined.

      state:   The current search state
               (a data structure you chose in your search problem)

      problem: The CornersProblem instance for this layout.

    This function should always return a number that is a lower bound on the
    shortest path from the state to a goal of the problem; i.e.  it should be
    admissible (as well as consistent).
    """
    corners = problem.corners  # These are the corner coordinates
    walls = problem.walls  # These are the walls of the maze, as a Grid (game.py)

    "*** YOUR CODE HERE ***"
    return 0  # Default to trivial solution


class AStarCornersAgent(SearchAgent):
    "A SearchAgent for FoodSearchProblem using A* and your foodHeuristic"

    def __init__(self):
        self.searchFunction = lambda prob: search.aStarSearch(prob, cornersHeuristic)
        self.searchType = CornersProblem


class FoodSearchProblem:
    """
    A search problem associated with finding the a path that collects all of the
    food (dots) in a Pacman game.

    A search state in this problem is a tuple ( pacmanPosition, foodGrid ) where
      pacmanPosition: a tuple (x,y) of integers specifying Pacman's position
      foodGrid:       a Grid (see game.py) of either True or False, specifying remaining food
    """

    def __init__(self, startingGameState):
        self.start = (startingGameState.getPacmanPosition(), startingGameState.getFood())
        self.walls = startingGameState.getWalls()
        self.startingGameState = startingGameState
        self._expanded = 0  # DO NOT CHANGE
        self.heuristicInfo = {}  # A dictionary for the heuristic to store information

    def getStartState(self):
        return self.start

    def isGoalState(self, state):
        return state[1].count() == 0

    def getSuccessors(self, state):
        "Returns successor states, the actions they require, and a cost of 1."
        successors = []
        self._expanded += 1  # DO NOT CHANGE
        for direction in [Directions.NORTH, Directions.SOUTH, Directions.EAST, Directions.WEST]:
            x, y = state[0]
            dx, dy = Actions.directionToVector(direction)
            nextx, nexty = int(x + dx), int(y + dy)
            if not self.walls[nextx][nexty]:
                nextFood = state[1].copy()
                nextFood[nextx][nexty] = False
                successors.append((((nextx, nexty), nextFood), direction, 1))
        return successors

    def getCostOfActions(self, actions):
        """Returns the cost of a particular sequence of actions.  If those actions
        include an illegal move, return 999999"""
        x, y = self.getStartState()[0]
        cost = 0
        for action in actions:
            # figure out the next state and see whether it's legal
            dx, dy = Actions.directionToVector(action)
            x, y = int(x + dx), int(y + dy)
            if self.walls[x][y]:
                return 999999
            cost += 1
        return cost


class AStarFoodSearchAgent(SearchAgent):
    "A SearchAgent for FoodSearchProblem using A* and your foodHeuristic"

    def __init__(self):
        self.searchFunction = lambda prob: search.aStarSearch(prob, foodHeuristic)
        self.searchType = FoodSearchProblem


def foodHeuristic(state, problem):
    """
    Your heuristic for the FoodSearchProblem goes here.

    This heuristic must be consistent to ensure correctness.  First, try to come
    up with an admissible heuristic; almost all admissible heuristics will be
    consistent as well.

    If using A* ever finds a solution that is worse uniform cost search finds,
    your heuristic is *not* consistent, and probably not admissible!  On the
    other hand, inadmissible or inconsistent heuristics may find optimal
    solutions, so be careful.

    The state is a tuple ( pacmanPosition, foodGrid ) where foodGrid is a Grid
    (see game.py) of either True or False. You can call foodGrid.asList() to get
    a list of food coordinates instead.

    If you want access to info like walls, capsules, etc., you can query the
    problem.  For example, problem.walls gives you a Grid of where the walls
    are.

    If you want to *store* information to be reused in other calls to the
    heuristic, there is a dictionary called problem.heuristicInfo that you can
    use. For example, if you only want to count the walls once and store that
    value, try: problem.heuristicInfo['wallCount'] = problem.walls.count()
    Subsequent calls to this heuristic can access
    problem.heuristicInfo['wallCount']
    """
    position, foodGrid = state
    "*** YOUR CODE HERE ***"
    return 0


class ClosestDotSearchAgent(SearchAgent):
    "Search for all food using a sequence of searches"

    def registerInitialState(self, state):
        self.actions = []
        currentState = state
        while (currentState.getFood().count() > 0):
            nextPathSegment = self.findPathToClosestDot(currentState)  # The missing piece
            self.actions += nextPathSegment
            for action in nextPathSegment:
                legal = currentState.getLegalActions()
                if action not in legal:
                    t = (str(action), str(currentState))
                    raise Exception, 'findPathToClosestDot returned an illegal move: %s!\n%s' % t
                currentState = currentState.generateSuccessor(0, action)
        self.actionIndex = 0
        print 'Path found with cost %d.' % len(self.actions)

    def findPathToClosestDot(self, gameState):
        """
        Returns a path (a list of actions) to the closest dot, starting from
        gameState.
        """
        # Here are some useful elements of the startState
        startPosition = gameState.getPacmanPosition()
        food = gameState.getFood()
        walls = gameState.getWalls()
        problem = AnyFoodSearchProblem(gameState)

        "*** YOUR CODE HERE ***"
        util.raiseNotDefined()


class AnyFoodSearchProblem(PositionSearchProblem):
    """
    A search problem for finding a path to any food.

    This search problem is just like the PositionSearchProblem, but has a
    different goal test, which you need to fill in below.  The state space and
    successor function do not need to be changed.

    The class definition above, AnyFoodSearchProblem(PositionSearchProblem),
    inherits the methods of the PositionSearchProblem.

    You can use this search problem to help you fill in the findPathToClosestDot
    method.
    """

    def __init__(self, gameState):
        "Stores information from the gameState.  You don't need to change this."
        # Store the food for later reference
        self.food = gameState.getFood()

        # Store info for the PositionSearchProblem (no need to change this)
        self.walls = gameState.getWalls()
        self.startState = gameState.getPacmanPosition()
        self.costFn = lambda x: 1
        self._visited, self._visitedlist, self._expanded = {}, [], 0  # DO NOT CHANGE

    def isGoalState(self, state):
        """
        The state is Pacman's position. Fill this in with a goal test that will
        complete the problem definition.
        """
        x, y = state

        "*** YOUR CODE HERE ***"
        util.raiseNotDefined()


def mazeDistance(point1, point2, gameState):
    """
    Returns the maze distance between any two points, using the search functions
    you have already built. The gameState can be any game state -- Pacman's
    position in that state is ignored.

    Example usage: mazeDistance( (2,4), (5,6), gameState)

    This might be a useful helper function for your ApproximateSearchAgent.
    """
    x1, y1 = point1
    x2, y2 = point2
    walls = gameState.getWalls()
    assert not walls[x1][y1], 'point1 is a wall: ' + str(point1)
    assert not walls[x2][y2], 'point2 is a wall: ' + str(point2)
    prob = PositionSearchProblem(gameState, start=point1, goal=point2, warn=False, visualize=False)
    return len(search.bfs(prob))

class HashableDict:
    def __init__(self, dict):
        self.dict = dict
        self.str  = str(dict)
        self.hash = self.str.__hash__()
    def __eq__(self, other):
        return self.str.__eq__( other.str )
    def __hash__(self):
        return self.hash
    def __getitem__(self, i):
        return self.dict.__getitem__(i)
    def __setitem__(self, i, v):
        return self.dict.__setitem__(i, v)
    def __str__(self):
        return self.dict.__str__()

class CornersAndCapsulesProblem(search.SearchProblem):
    def __init__(self, startingGameState):
        """
        You will find the following functions useful:
        startingGameState.getCapsules()
        startingGameState.getWalls()
        startingGameState.getFood()
        startingGameState.getPacmanPosition()
        """
        self.walls = startingGameState.getWalls().data
        self.startingState = HashableDict({
            'pacmanPosition': startingGameState.getPacmanPosition(),
            'food': set( startingGameState.getFood().asList() ),
            'capsules': set( startingGameState.getCapsules() )
        })

        self.currentStateId = 0

        self._expanded = 0  # DO NOT CHANGE; Number of search nodes expanded

        if cornersAndCapsulesHeuristic == longestDistanceMazeHeuristic:
            self.findingAllTwoShortestPaths()

        self.bfs_memoize = {}
        self.spanningTree_memoize = {}

    def getStartState(self):
        """
        Returns the start state (in your state space, not the full Pacman state
        space)
        """
        return self.startingState

    def isGoalState(self, state):
        """
        Returns whether this search state is a goal state of the problem.
        """
        return len(state['food']) == 0 and len(state['capsules']) == 0

    def getSuccessors(self, state):
        """
        Returns successor states, the actions they require, and a cost of 1.

         As noted in search.py:
            For a given state, this should return a list of triples, (successor,
            action, stepCost), where 'successor' is a successor to the current
            state, 'action' is the action required to get there, and 'stepCost'
            is the incremental cost of expanding to that successor
        """
        _directions = {Directions.NORTH: (0, 1),
                       Directions.SOUTH: (0, -1),
                       Directions.EAST:  (1, 0),
                       Directions.WEST:  (-1, 0)}

        #import time
        #time.sleep(.4)
        #self.printState(state)
        #print state
        #print
        #print state.__hash__()

        successors = []
        x, y = state['pacmanPosition']

        for action in [Directions.NORTH, Directions.SOUTH, Directions.EAST, Directions.WEST]:
            dx, dy = _directions[action]
            nextx, nexty = int(x + dx), int(y + dy)
            nextPosition = (nextx, nexty)
            hitsWall = self.walls[nextx][nexty]

            hitsFoodBeforeCapsule = len(state['capsules']) > 0 and nextPosition in state['food']

            if not hitsWall and not hitsFoodBeforeCapsule:
                if nextPosition in state['food']:
                    nextFoodState = state['food'].copy()
                    nextFoodState.discard(nextPosition)
                else:
                    nextFoodState = state['food']

                if nextPosition in state['capsules']:
                    nextCapsulesState = state['capsules'].copy()
                    nextCapsulesState.discard(nextPosition)
                else:
                    nextCapsulesState = state['capsules']

                self.currentStateId += 1
                nextState = HashableDict({
                    'pacmanPosition': (nextx, nexty),
                    'food': nextFoodState,
                    'capsules': nextCapsulesState
                })

                successors.append( (nextState, action, 1) )

        self._expanded += 1  # DO NOT CHANGE
        return successors

    def getCostOfActions(self, actions):
        """
        Returns the cost of a particular sequence of actions.  If those actions
        include an illegal move, return 999999.  This is implemented for you.
        """
        #print actions
        # TODO: correct! it is an illegal move to take any food before all capsules
        if actions == None: return 999999
        x, y = self.startingState['pacmanPosition']
        for action in actions:
            dx, dy = Actions.directionToVector(action)
            x, y = int(x + dx), int(y + dy)
            if self.walls[x][y]: return 999999
        return len(actions)

    def printState(self, state):
        for y in range(len(self.walls[0])-1,-1,-1):
            line = ""
            for x in range(len(self.walls)):
                if self.walls[x][y]:
                    line += '%'
                elif state['pacmanPosition'] == (x,y):
                    line += 'P'
                elif (x,y) in state['food']:
                    line += '.'
                elif (x,y) in state['capsules']:
                    line += 'o'
                else:
                    line += ' '
            print line

    def findingAllShortestPaths(self, areFoodWalls):
        height = len(self.walls[0])
        width  = len(self.walls)
        inf    = float('inf')

        #self.printState(self.startingState)
        #print "height and width:", height, width

        movements = [(1,0), (0,1), (-1,0), (0,-1)]

        legalPositions = [ (x,y) for x in range(width)
                                 for y in range(height)
                                 if not self.walls[x][y] ]

        # removing food positions from legal positions, food are treated as walls
        if areFoodWalls:
            legalPositions = [ pos for pos in legalPositions if not pos in self.startingState['food'] ]

        #print "Number of legal positions:", len(legalPositions)
        n = len(legalPositions)

        shortestDistance = []
        # matrix that contains the shortest distance between any pair of positions at a 0 distance
        shortestDistance.append( [ [ (0 if startPos==endPos else inf) for endPos in legalPositions ] for startPos in legalPositions ] )

        # have been new shorter distances found, if not, stop
        changed = True

        # Implementation of All-pairs shortest path problem using the Floyd-Warshall algorithm
        # See: https://en.wikipedia.org/wiki/Floyd%E2%80%93Warshall_algorithm
        while changed:
            changed = False
            shortestDistance.append( [ row[:] for row in shortestDistance[-1] ] )
            for iStartPos in range(n):
                startPosX, startPosY = legalPositions[iStartPos]
                for iEndPos in range(n):
                    endPosX, endPosY = legalPositions[iEndPos]

                    for dx, dy in movements:
                        nextPosX, nextPosY = endPosX+dx, endPosY+dy
                        if (nextPosX,nextPosY) in legalPositions:
                            iNextPos = legalPositions.index( (nextPosX,nextPosY) )
                            newDistanceFromStartToNextPosition = shortestDistance[-2][iStartPos][iEndPos] + 1

                            if newDistanceFromStartToNextPosition < shortestDistance[-1][iStartPos][iNextPos]:
                                shortestDistance[-1][iStartPos][iNextPos] = newDistanceFromStartToNextPosition
                                changed = True

            #print "Iterations of Floyd-Warshall:", len(shortestDistance)

        #print "Number of iterations of Floyd-Warshall:", len(shortestDistance)

        return shortestDistance[-1], dict( [(legalPositions[i],i) for i in range(len(legalPositions))] )

    def findingAllTwoShortestPaths(self):
        shortestPathsCapsules, legalPositions = self.findingAllShortestPaths(True)
        self.legalPositionsCapsules = legalPositions
        self.shortestPathsCapsules  = shortestPathsCapsules

        shortestPathsFood, legalPositions = self.findingAllShortestPaths(False)
        self.legalPositionsFood = legalPositions
        self.shortestPathsFood  = shortestPathsFood

    def distanceBetweenTwoPositions(self, pos1, pos2, areFoodWalls):
        if areFoodWalls:
            iPos1 = self.legalPositionsCapsules[pos1]
            iPos2 = self.legalPositionsCapsules[pos2]
            return self.shortestPathsCapsules[iPos1][iPos2]
        else:
            iPos1 = self.legalPositionsFood[pos1]
            iPos2 = self.legalPositionsFood[pos2]
            return self.shortestPathsFood[iPos1][iPos2]

    def bfs(self, position, targets, food=None):
        # print "position, targets:", position, targets
        if (position, food is None) in self.bfs_memoize:
            bfs_graph = self.bfs_memoize[ (position, food is None) ]
        else:
            bfs_graph = [[99999 for i in row] for row in self.walls]
            startX, startY = position
            bfs_graph[startX][startY] = 0

            frontier = Queue()
            frontier.put( position )

            #import time
            #start = time.time()
            #totalexecutions = 0

            while not frontier.empty():
                #totalexecutions += 1
                x, y = frontier.get()

                #print "iteration", totalexecutions
                #print (x,y), "from the frontier"
                #time.sleep(.14)

                if self.walls[x][y] or (food is not None and (x,y) in food): continue

                depth = bfs_graph[x][y]
                for dx, dy in [(1,0), (0,1), (-1,0), (0,-1)]:
                    if not self.walls[x+dx][y+dy] and bfs_graph[x+dx][y+dy] > depth+1:
                        #print (x+dx, y+dy), "added to the frontier"
                        frontier.put( (x+dx, y+dy) )
                        bfs_graph[x+dx][y+dy] = depth+1
                #print

            #end = time.time()
            #print "time expent:", (end-start), "total exec:", totalexecutions, "food: ", food
            self.bfs_memoize[ (position, food is None) ] = bfs_graph

        return [ bfs_graph[x][y] for x,y in targets ]

    def valueOfMininumSpanningTreeOf(self, positions, food=None):
        if tuple(positions) in self.spanningTree_memoize:
            return self.spanningTree_memoize[ tuple(positions) ]
        else:
            graph = { node: {} for node in positions }
            n = len(positions)

            for i in range(n):
                nodesFromNodeI = positions[:]
                nodeI = nodesFromNodeI.pop(i)

                distancesFromNodeI = self.bfs(nodeI, nodesFromNodeI, food)
                graph[nodeI] = { nodesFromNodeI[j]: distancesFromNodeI[j] for j in range(n-1) }

            result = sum( [ graph[node1][node2] for node1, node2 in MinimumSpanningTree(graph) ] )
            self.spanningTree_memoize[ tuple(positions) ] = result
            return result

def longestDistanceMazeHeuristic(state, problem):
    capsules = state['capsules']
    food = state['food']
    pacmanPosition = state['pacmanPosition']

    if len(capsules) > 0:
        distances = []
        for capPos in capsules:
            distances.append( reduce(max,
                                     [problem.distanceBetweenTwoPositions(capPos, foodPos, False)
                                        for foodPos in food],
                                     0
                                    )
                             + problem.distanceBetweenTwoPositions(pacmanPosition, capPos, True)
                             )
    # Now, there is only food left to eat
    else:
        # Distances from pacman to all capsules and food
        distances = [problem.distanceBetweenTwoPositions(pacmanPosition, foodPos, False) for foodPos in food]

    return reduce( max, distances, 0 )

# same algorithm as before, but the computation of the shortest paths is done at the heuristic time
def longestDistanceMazeHeuristic2(state, problem):
    capsules = list(state['capsules'])
    food = state['food']
    pacmanPosition = state['pacmanPosition']

    if len(capsules) > 0:
        distances = []

        distanceFromPacman = problem.bfs(pacmanPosition, capsules, food)
        for i in range(len(capsules)):
            capPos = capsules[i]
            distances.append( reduce(max, problem.bfs(capPos, list(food)), 0)
                             + distanceFromPacman[i]
                             )
    # Now, there is only food left to eat
    else:
        # Distances from pacman to all capsules and food
        distances = problem.bfs(pacmanPosition, list(food))

    return reduce( max, distances, 0 )

def minimumSpanningTreeHeuristic(state, problem):
    capsules = list(state['capsules'])
    food = state['food']
    pacmanPosition = state['pacmanPosition']

    if len(capsules) > 0:
        minDistanceFromCapsules = min( [ problem.valueOfMininumSpanningTreeOf( [capsule] + list(food) ) for capsule in capsules ] )
        return problem.valueOfMininumSpanningTreeOf( [pacmanPosition] + list(capsules), food ) + minDistanceFromCapsules

    # Now, there is only food left to eat
    else:
        return problem.valueOfMininumSpanningTreeOf( [pacmanPosition] + list(food) )


cornersAndCapsulesHeuristic = minimumSpanningTreeHeuristic

"""
Test your code with this agent
python pacman.py -l tinyMaze -p AStarCornersAndCapsulesAgent
"""


class AStarCornersAndCapsulesAgent(SearchAgent):
    def __init__(self):
        self.searchFunction = lambda prob: search.astar(prob, cornersAndCapsulesHeuristic)
        self.searchType = CornersAndCapsulesProblem
